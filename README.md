# Dice App
## Goal
The purpose of this application is that this application shows how I build applications with functionality
using setState () within Stateful Flutter widgets.

## What is the app for?
This app rolls two random dice. You can make the die roll at the press of a button. With this app in your pocket, you'll be able to settle any score on the go!


## What We Have Done
- How to use Flutter stateless widgets to design the user interface.
- How to use Flutter stateful widgets to update the user interface.
- How to change the properties of various widgets.
- How to use onClick listeners to detect when buttons are pressed.
- How to use setState to mark the widget tree as dirty and requiring update on the next render.
- How to use expanded to make widgets adapt to screen dimensions.
- Understand and use string interpolation.
- How app icon add.
- Learn about basic dart programming concepts such as data types and functions.
- Code and use gesture controls.
